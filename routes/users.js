// USERS
// POST /users
app.post('/users', async (req, res) => {

    try {

        if(!req.files || !req.body.email || !req.body.password) {
            throw new Error('All fields are required');
        }
    
        const body = _.pick(req.body, ['email', 'password']);
    
        const image = req.files.image;
        const imageName = `${new ObjectID().toHexString()}_${image.name}`;
        const imageLink = `/images/${imageName}`;
    
        const user = new User({
            email: body.email,
            password: body.password,
            image: imageLink
        }); 

        await Promise.all([
            image.mv(path.join(__dirname, imageLink)),
            user.save()
        ])

        const token = await user.generateAuthToken(); // test if token is a string, NOT a promise

        res.header('X-Auth', token).send(user);
    } catch (err) {
        res.status(400).send(err.message);
    }

});

//GET /users - it should return all users

app.get('/users', async (req, res) => {
  
    try {

        const page = req.query.page ? +req.query.page : 1;
        const sort = req.query.sort ? req.query.sort : 'desc';
        const limit = req.query.limit ? +req.query.limit : 2;

        const users = await User.paginate({}, { page: page, sort: { _id: sort }, limit: limit });

        res.status(200).send({
            users: users.docs,
            meta: {
                total: users.total,
                page: users.page,
                pages: users.pages,
                limit: users.limit
            }
        });

    } catch (error) {
        return res.status(400).send();
    }

});

// GET /users/me
app.get('/users/me', authenticate, async (req, res) => {
    res.send(req.user);
});

// POST /users/login
app.post('/users/login', async (req, res) => {

    try {
        const body = _.pick(req.body, ['email', 'password']);
        const user = await User.findByCredentials(body.email, body.password);
        
        if(!user) {
            throw new Error('Bad credentials');
        }

        const token = await user.generateAuthToken();

        res.header('X-Auth', token).send(user);


    } catch (error) {
        return res.status(400).send(error.message);
    }

});

// DELETE /users/me/token
app.delete('/users/me/token', authenticate, async (req, res) => {

    try {
        await req.user.removeToken(req.token)
        res.status(200).send();

    } catch (error) {
        return res.status(400).send();
    }

});

// DELETE /users/me
app.delete('/users/me', authenticate, async (req, res) => {

    try {
        const user = await User.findOneAndRemove({_id: req.user._id});

        if(!user) {
            throw new Error('User NOT found');
        }

        res.send(req.user);

    } catch (error) {
        res.status(400).send(err.message);
    }
    
});

// PATCH /users/me/update
app.patch('/users/me/update', authenticate, async (req, res) => {

    try {

        const body = _.pick(req.body, ['email', 'description', 'name', 'password']);
        const user = req.user;

        let image;
        if (req.files && req.files.image) {      
            
            fs.unlink(path.join(__dirname, user.image), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            image = req.files.image;
            const imageName = `${new ObjectID().toHexString()}_${image.name}`;
            const imageLink = `/images/${imageName}`;
            
            body.image = imageLink;

        }

        await user.set(body);
        await user.save();
        if (image) {
            await image.mv(path.join(__dirname, body.image));
        }
    
        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }
    
});

// DELETE /users/:id
app.delete('/users/:id', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to delete this user');
        }
    
        const user = await User.findOneAndRemove({_id: req.params.id});

        if(!user) {
            throw new Error('User NOT found');
        }

        fs.unlink(path.join(__dirname, user.image), (err) => {
            if (err) {
                console.log(err);         
            }
        });

        const galleries = await Gallery.find({ _owner: user._id });

        galleries.forEach( async (gallery) => {

            gallery.images.forEach(image => {
                fs.unlink(path.join(__dirname, image.link), (err) => {
                    if (err) {
                        console.log(err);         
                    }
                });
            });

            await gallery.remove();
        });

        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /users/:id
app.patch('/users/:id', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to update this user');
        }

        const body = _.pick(req.body, ['email', 'admin', 'description', 'name', 'password']);

        const user = await User.findOne({_id: req.params.id});

        if(!user) {
            throw new Error('User NOT found');
        }

        let image;
        if (req.files && req.files.image) {      
            
            fs.unlink(path.join(__dirname, user.image), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            image = req.files.image;
            const imageName = `${new ObjectID().toHexString()}_${image.name}`;
            const imageLink = `/images/${imageName}`;
            
            body.image = imageLink;

        }

        await user.set(body);
        await user.save();
        if (image) {
            await image.mv(path.join(__dirname, body.image));
        }

        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }

});


// GET /users/:id
app.get('/users/:id', async (req, res) => {

    try {
    
        const user = await User.findOne({_id: req.params.id});

        if(!user) {
            throw new Error('User NOT found');
        }

        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }

});