const { ObjectID } = require('mongodb');
const jwt = require('jsonwebtoken');
const path = require('path');
const fs = require('fs').promises;

const { User } = require('./../../models/user');
const { Section } = require('./../../models/section');
const { News } = require('./../../models/news');
const { Gallery } = require('./../../models/gallery');

// *** IMAGES ***

async function populateImages() {
    this.timeout(5000);
    const restoreImgOne = fs.copyFile(path.join(__dirname, '../../images-test/seed-img.png'), 
                                      path.join(__dirname, '../../images-test/test-img.png'));
    const restoreImgTwo = fs.copyFile(path.join(__dirname, '../../images-test/seed-img.png'), 
                                      path.join(__dirname, '../../images-test/test-img2.png'));
    await Promise.all([restoreImgOne, restoreImgTwo]);
}

const directory = path.join(__dirname, '..', '..', 'images');

async function clearImages() {
    try {

        const files = await fs.readdir(directory);

        for (const file of files) {
            await fs.unlink(path.join(directory, file));
        }

    } catch (error) {
        throw new Error(error);
    }
}



// *** USERS ***

const userOneId = new ObjectID();
const userTwoId = new ObjectID();


const users = [
    {
        _id: userOneId,
        email: 'test@gmail.test',
        password: 'superSecret1',
        admin: true,
        name: 'Elen',
        image: '/images-test/test-img.png',
        tokens: [
            {
                access: 'auth',
                token: jwt.sign({
                    _id: userOneId.toHexString(),
                    access: 'auth',
                }, process.env.JWT_SECRET).toString()
            }
        ]
    },
    {
        _id: userTwoId,
        email: 'test+newsletter@gmail.test',
        password: 'superSecret2',
        name: 'Mike',
        admin: false,
        image: '/images-test/test-img2.png',
        tokens: [
            {
                access: 'auth',
                token: jwt.sign({
                    _id: userTwoId.toHexString(),
                    access: 'auth'
                }, process.env.JWT_SECRET).toString()
            }
        ]
    },
];

async function populateUsers() {
    this.timeout(5000);
    await User.remove({});
    const userOne = new User(users[0]).save();
    const userTwo = new User(users[1]).save();
    await Promise.all([userOne, userTwo]);
};

// *** NEWS ***

const newsOneId = new ObjectID();
const newsTwoId = new ObjectID();

const news = [
    {
        _id: newsOneId,
        title: 'News One',
        content: 'Awesome cool content one',
        image: '/images-test/test-img.png',
        _owner: userOneId
    },
    {
        _id: newsTwoId,
        name: 'News Two',
        content: 'Awesome cool content two',
        image: '/images-test/test-img2.png',
        _owner: userTwoId
    }
];

async function populateNews() {
    this.timeout(5000);
    await News.remove({});
    const newsOne = new News(news[0]).save();
    const newsTwo = new News(news[1]).save();
    await Promise.all([newsOne, newsTwo]);
};


// *** GALLERY ***

const galleryOneId = new ObjectID();
const galleryTwoId = new ObjectID();

const galleries = [
    {
        _id: galleryOneId,
        name: 'Gallery One',
        description: 'Awesome cool description one',
        images : [
            {
                name: 'test-img.png',
                link: path.join('images-test', 'test-img2.png')
            }
        ],
        _owner: userTwoId
    },
    {
        _id: galleryTwoId,
        name: 'Gallery Two',
        description: 'Awesome cool description two',
        images : [
            {
                name: 'test-img2.png',
                link: path.join('images-test', 'test-img2.png')
            }
        ],
        _owner: newsOneId
    }
];

async function populateGalleries() {
    this.timeout(5000);
    await Gallery.remove({});
    const galleryOne = new Gallery(galleries[0]).save();
    const galleryTwo = new Gallery(galleries[1]).save();
    await Promise.all([galleryOne, galleryTwo]);
};


// *** SECTIONS ***

const sectionOneId = new ObjectID();
const sectionTwoId = new ObjectID();

const sections = [
    {
        _id: sectionOneId,
        name: 'Section One',
        title: 'Awesome cool title one',
        content: 'bla balab bal bla'
    },
    {
        _id: sectionTwoId,
        name: 'Section Two',
        title: 'Awesome cool title two',
        content: 'bla balab bal bla ha ha bla'
    }
];

async function populateSections() {
    this.timeout(5000);
    await Section.remove({});
    const sectionOne = new Section(sections[0]).save();
    const sectionTwo = new Section(sections[1]).save();
    await Promise.all([sectionOne, sectionTwo]);
};


module.exports = {
    populateImages,
    clearImages,

    users,
    populateUsers,

    news,
    populateNews,

    galleries,
    populateGalleries,

    sections,
    populateSections
};
