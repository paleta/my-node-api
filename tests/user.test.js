
const expect = require('expect');
const request = require('supertest');
const { ObjectID } = require('mongodb');
const path = require('path');
const { app } = require('./../server');
const fs = require('fs');

const { populateUsers, users, populateNews, news, populateGalleries, galleries, populateImages, clearImages, sections, populateSections } = require('./seed/seed');
const { User } = require('./../models/user');
const { Section } = require('./../models/section');
const { News } = require('./../models/news');
const { Gallery } = require('./../models/gallery');

beforeEach(clearImages);
beforeEach(populateImages);
beforeEach(populateImages);
beforeEach(populateUsers);
beforeEach(populateNews);
beforeEach(populateGalleries);
beforeEach(populateSections);

describe('USERS route', () => {

    describe('POST /users', () => {

        it('should register new user and return user with auth token if all data is correct', done => {

            const pathToImage = path.join(__dirname, '../images-test/test-img.png');
            const password = '123456!';
            const email = 'example@example.com';

            request(app)
                .post('/users')
                .attach('image', pathToImage)
                .set('X-Auth', users[0].tokens[0].token)
                .field('email', email)
                .field('password', password)
                .expect(200)
                .expect( res => {

                    expect(res.headers['x-auth']).toBeTruthy();
                    expect(res.body._id).toBeTruthy();
                    expect(res.body.email).toBe(email);
                    expect(res.body.image).toContain('test-img.png');

                })
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    User.findOne({email})
                        .then(user => {
                            expect(user).toBeTruthy();
                            expect(user.password === password).toBeFalsy();      
                            expect((fs.statSync(path.join(__dirname, '..', user.image ))).size > 0).toBeTruthy();           
                            done();
                        })
                        .catch(err => done(err));
                });

        });

        it('should return validation errors if request invalid', done => {

            request(app)
                .post('/users')
                .send({ email: 'ihoohoihoi', password: '123', image: 'gduiudgiu' })
                .set('X-Auth', users[0].tokens[0].token)
                .expect(400)
                .expect(res => {
                    // console.log(res.error);
                    
                    expect(typeof res.error.text).toBe('string');
                    expect(res.error.text.length > 0).toBeTruthy();
                })
                .end(done);

        });

        it('should return validation errors if email field are missing', done => {

            const pathToImage = path.join(__dirname, '../images-test/test-img.png');

            request(app)
                .post('/users')
                .field('password', 'duysfdyudfuyd')
                .set('X-Auth', users[0].tokens[0].token)
                .attach('image', pathToImage)
                .expect(400)
                .expect(res => {
                    // console.log(res.error);
                    
                    expect(typeof res.error.text).toBe('string');
                    expect(res.error.text.length > 0).toBeTruthy();
                })
                .end(done);

        });

        it('should return 401 Unauthorized if user is NOT logged in', done => {

            request(app)
                .post('/users')
                .send({ email: 'ihoohoihoi', password: '123434554', image: 'gduiudgiu' })
                .expect(401)
                .end(done);
        });

        it('should return 400 Bad Request if user is NOT an admin', done => {

            request(app)
                .post('/users')
                .send({ email: 'ihoohoihoi', password: '123434554', image: 'gduiudgiu' })
                .set('X-Auth', users[1].tokens[0].token)
                .expect(400)
                .expect(res => {
                    expect(typeof res.error.text).toBe('string');
                    expect(res.error.text.length > 0).toBeTruthy();
                })
                .end(done);
        });

    });

    describe('GET /users', () => {

        it('should get all users', done => {

            request(app)
                .get('/users')
                .expect(200)
                .expect(res => {
            
                    expect(res.body.users[0]._id).toBe(users[1]._id.toHexString());
                    expect(res.body.users[1]._id).toBe(users[0]._id.toHexString());
                    expect(res.body.users.length).toBe(2);

                    expect(res.body.users[0].email).toBe(users[1].email);
                    expect(res.body.users[1].email).toBe(users[0].email);
        
                })
                .end(done);

        });

        it('should return aditional pagination info', done => {
            request(app)
                .get('/users?limit=20')
                .expect(200)
                .expect(res => {         
                    expect(res.body.meta).toMatchObject({
                        total: res.body.users.length,
                        page: 1,
                        pages: 1,
                        limit: 20
                    });           
                })
                .end(done);

        });

    });

    describe('GET /users/me', () => {

        it('should get authorized user with his data', done => {

            request(app)
                .get('/users/me')
                .set('X-Auth', users[0].tokens[0].token)
                .expect(200)
                .expect(res => {
                    expect(res.body).toMatchObject({
                        name: users[0].name,
                        _id: users[0]._id.toHexString(),
                        admin: users[0].admin,
                        image: users[0].image
                    });
                })
                .end(done);
        });

        it('should NOT get authorized user with his data if user is UNAUTHENTICATED', done => {

            request(app)
                .get('/users/me')
                .expect(401)
                .end(done);
        });

    });

    describe('POST /users/login', () => {

        it('should create and return new auth token with user', done => {

            request(app)
                .post('/users/login')
                .send({ email: users[0].email, password: users[0].password })
                .expect(200)
                .expect(res => {

                    expect(!!res.headers['x-auth']).toBeTruthy();
                    expect(!!res.body._id).toBeTruthy();
                    
                })
                .end(async (err, res) => {
                    if (err) {
                        return done(err);
                    }

                    try {

                        const user = await User.findById(users[0]._id)
                        expect(user.tokens[1]).toMatchObject({
                                    access: 'auth',
                                    token: res.headers['x-auth']
                        });
                        done();

                    } catch (error) {
                        done(error);
                    }


                });

        });

        it('should NOT create and return new auth token with user when password is wrong', done => {

            request(app)
                .post('/users/login')
                .send({ email: users[0].email, password: users[1].password })
                .expect(400)
                .end(done);
            
        });

    });

    describe('DELETE /users/me/token', () => {

        it('should delete user token', done => {

            request(app)
                .delete('/users/me/token')
                .set('X-Auth', users[0].tokens[0].token)         
                .expect(200)
                .end(async (err, res) => {
                
                    if (err) {
                        return done(err);
                    }

                    try {

                        const user = await User.findOne({ _id: users[0]._id });             
                        expect([...user.tokens].length).toBe(0);
                        done(); 

                    } catch (error) {

                        done(error); 

                    }
                });
        });

        it('should NOT delete user token if user is UNAUTHENTICATED', done => {

            request(app)
                .delete('/users/me/token')
                .expect(401)
                .end(done);
            
        });

    });

    describe('DELETE /users/me', () => {

        it('should delete user', done => {

            request(app)
                .delete('/users/me')
                .set('X-Auth', users[1].tokens[0].token)         
                .expect(200)
                .expect(res => {
                    expect(res.body._id).toBe(users[1]._id.toHexString());
      
                })
                .end(async (err, res) => {
                
                    if (err) {
                        return done(err);
                    }

                    try {

                        const user = await User.findOne({ _id: users[1]._id });             
                        expect(user).toBeFalsy();
                        done();
                        


                    } catch (error) {

                        done(error); 

                    }
                });
        });

        it('should NOT delete user if user is UNAUTHENTICATED', done => {

            request(app)
                .delete('/users/me')
                .expect(401)
                .end(done);
            
        });

    });

    describe('PATCH /users/me/update', () => {

        it('should update authorized user. if new image is provided, it should delete previous image.', done => {

            const pathToNewImage = path.join(__dirname, '../images-test/test-img2.png');

            User.findOne({ _id: users[1]._id })
                .then(user => {

                    const pathToPreviousImage = user.image;

                    const name = 'New name';
                    const desc = 'New cool description';


                    request(app)
                    .patch('/users/me/update')
                    .set('X-Auth', users[1].tokens[0].token)   
                    .attach('image', pathToNewImage)
                    .field('name', name)
                    .field('description', desc)      
                    .expect(200)
                    .expect(res => {
        
                        expect(res.body.name).toBe(name);
                        expect(res.body.description).toBe(desc);
                        expect(res.body.image).toContain('test-img2.png');
                        expect(res.body.image === pathToPreviousImage).toBeFalsy();
                      
                
                    }).end(done);

                })
                .catch(e => done(e));

        });

        it('should NOT update user if user is UNAUTHENTICATED', done => {

            request(app)
                .patch('/users/me/update')
                .send({ name: 'name' })
                .expect(401)
                .end(done);
            
        });


    });

    describe('DELETE /users/:id', () => {

        it('should allow admin to remove other user by id', done => {

            request(app)
                .delete('/users/' + users[1]._id)
                .set('X-Auth', users[0].tokens[0].token)   
                .expect(200)
                .end(done);

        });

        it('should NOT allow regular user to remove admin by id', done => {

            request(app)
                .delete('/users/' + users[0]._id)
                .set('X-Auth', users[1].tokens[0].token)   
                .expect(400)
                .end(done);


        });

        it('should NOT allow unauthorized user to remove someone by id', done => {

            request(app)
                .delete('/users/' + users[0]._id)
                .expect(401)
                .end(done);

                
        });

    });

    describe('PATCH /users/:id', () => {


        it('should allow admin to update other user by id.', done => {

            const pathToNewImage = path.join(__dirname, '../images-test/test-img2.png');

            User.findOne({ _id: users[1]._id })
                .then(user => {

                    const pathToPreviousImage = user.image;

                    const name = 'New name 2';
                    const desc = 'New cool description 2';


                    request(app)
                    .patch('/users/' + users[1]._id)
                    .set('X-Auth', users[0].tokens[0].token)   
                    .attach('image', pathToNewImage)
                    .field('name', name)
                    .field('description', desc)      
                    .expect(200)
                    .expect(res => {
        
                        expect(res.body.name).toBe(name);
                        expect(res.body.description).toBe(desc);
                        expect(res.body.image).toContain('test-img2.png');
                        expect(res.body.image === pathToPreviousImage).toBeFalsy();
                        

                
                    }).end(done);

                })
                .catch(e => done(e));

        });

        it('should NOT allow regular user to update admin by id', done => {

            request(app)
                .patch('/users/' + users[0]._id)
                .set('X-Auth', users[1].tokens[0].token)   
                .send({ description: 'something '})
                .expect(400)
                .end(done);


        });

        it('should NOT allow unauthorized user to update someone by id', done => {

            request(app)
                .patch('/users/' + users[0]._id)
                .send({ description: 'something '})
                .expect(401)
                .end(done);

                
        });

    });

    describe('GET /users/:id', () => {

        it('should get user by id', done => {

            request(app)
                .get('/users/' + users[0]._id)
                .expect(200)
                .expect(res => {
            
                    expect(res.body.name).toBe(users[0].name);
                    expect(res.body.email).toBe(users[0].email);
                    expect(res.body._id).toBe(users[0]._id.toHexString());

        
                })
                .end(done);

        });

        it('should return 400 if ID is NOT ObjectID', done => {

            request(app)
                .get('/users/123')
                .expect(400)
                .end(done);

        });

        it('should return 400 if there is no user with given id', done => {

            request(app)
                .get('/users/' + new ObjectID().toHexString())
                .expect(400)
                .end(done);

        });

    });

});

describe('SECTION route', () => {

    describe('POST /sections', () => {

        it('should allow admin to add new section', done => {

            const title = 'New title';
            const content = 'bla bla bla';
            const name = 'price_list';

            request(app)
                .post('/sections')
                .set('X-Auth', users[0].tokens[0].token)   
                .send({ title, content, name })
                .expect(200)
                .expect(res => {
                    expect(res.body).toMatchObject({
                        title,
                        content,
                        name
                    });
                })
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    Section.findOne({ name })
                        .then(section => {
                            expect(section).toBeTruthy();
                            expect(section).toMatchObject({
                                title,
                                content,
                                name
                            });
                            done();
                        })
                        .catch(err => done(err));
                });

        });

        it('should NOT allow regular user to add new section', done => {

            const title = 'New title';
            const content = 'bla bla bla';
            const name = 'price_list';

            request(app)
                .post('/sections')
                .set('X-Auth', users[1].tokens[0].token)   
                .send({ title, content, name })
                .expect(400)
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    Section.findOne({ name })
                        .then(section => {
                            expect(section).toBeFalsy();
                            done();
                        })
                        .catch(err => done(err));
                });

        });

        it('should NOT allow unauthorized user to add new section', done => {

            const title = 'New title';
            const content = 'bla bla bla';
            const name = 'price_list';

            request(app)
                .post('/sections') 
                .send({ title, content, name })
                .expect(401)
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    Section.findOne({ name })
                        .then(section => {
                            expect(section).toBeFalsy();
                            done();
                        })
                        .catch(err => done(err));
                });

        });

    });

    describe('GET /sections', () => {

    
        it('should get all sections', done => {

            request(app)
                .get('/sections')
                .expect(200)
                .expect(res => {
            
                    expect(res.body.sections[0]._id).toBe(sections[1]._id.toHexString());
                    expect(res.body.sections[1]._id).toBe(sections[0]._id.toHexString());
                    expect(res.body.sections.length).toBe(2);

                    expect(res.body.sections[0].name).toBe(sections[1].name);
                    expect(res.body.sections[1].name).toBe(sections[0].name);
        
                })
                .end(done);

        });


        it('should get sections with pagination info', done => {

            request(app)
                .get('/sections?limit=40')
                .expect(200)
                .expect(res => {         
                    expect(res.body.meta).toMatchObject({
                        total: res.body.sections.length,
                        page: 1,
                        pages: 1,
                        limit: 40
                    });           
                })
                .end(done);
        });
    });


    describe('DELETE /sections', () => {
  
        it('should allow admin to delete section by id', done => {

            request(app)
                .delete('/sections/' + sections[0]._id.toHexString())
                .set('X-Auth', users[0].tokens[0].token)   
                .expect(200)
                .expect(res => {
                    expect(res.body).toMatchObject({
                        ...sections[0],
                        _id: sections[0]._id.toHexString(),
                    });
        
                })
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    Section.findOne({ _id: sections[0]._id.toHexString() })
                        .then(section => {
                            expect(section).toBeFalsy();
                            done();
                        })
                        .catch(err => done(err));
                });

        });

  
        it('should NOT allow regular user to delete section by id', done => {

            request(app)
                .delete('/sections/' + sections[0]._id.toHexString())
                .set('X-Auth', users[1].tokens[0].token)   
                .expect(400)
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    Section.findOne({ _id: sections[0]._id.toHexString() })
                        .then(section => {
                            expect(section).toBeTruthy();
                            done();
                        })
                        .catch(err => done(err));
                });

        });

        it('should NOT allow unauthorized user to delete section by id', done => {

            request(app)
                .delete('/sections/' + sections[0]._id.toHexString())
                .expect(401)
                .end(err => {
                    if (err) {
                        return done(err)
                    }

                    Section.findOne({ _id: sections[0]._id.toHexString() })
                        .then(section => {
                            expect(section).toBeTruthy();
                            done();
                        })
                        .catch(err => done(err));
                });

        });
    });

    describe('PATCH /sections', () => {


        it('should allow admin to patch section by id', done => {


            const title = 'Super New title';
            const content = 'Mega bla bla bla';
            const name = 'price_list_second';
    
            request(app)
                .patch('/sections/' + sections[1]._id.toHexString())
                .set('X-Auth', users[0].tokens[0].token)   
                .send({ title, content, name })
                .expect(200)
                .expect(res => {
                    expect(res.body).toMatchObject({
                        title,
                        content,
                        name
                    });
                })
                .end(err => {
                    if (err) {
                        return done(err)
                    }
    
                    Section.findOne({ _id: sections[1]._id.toHexString() })
                        .then(section => {
                            expect(section).toBeTruthy();
                            expect(section).toMatchObject({
                                title,
                                content,
                                name
                            });
                            done();
                        })
                        .catch(err => done(err));
                });
        });

        it('should NOT allow reqular user to patch section by id', done => {

            
            const title = 'Super New title';
            const content = 'Mega bla bla bla';
            const name = 'price_list_second';
    
            request(app)
                .patch('/sections/' + sections[1]._id.toHexString())
                .set('X-Auth', users[1].tokens[0].token)   
                .send({ title, content, name })
                .expect(400)
                .end(err => {
                    if (err) {
                        return done(err)
                    }
    
                    Section.findOne({ _id: sections[1]._id.toHexString() })
                        .then(section => {
                            expect(section).toBeTruthy();
                            expect(section).not.toMatchObject({
                                title,
                                content,
                                name
                            });
                            done();
                        })
                        .catch(err => done(err));
                });
        });

        
        it('should NOT allow unauthorized user to patch section by id', done => {

            
            const title = 'Super New title';
            const content = 'Mega bla bla bla';
            const name = 'price_list_second';
    
            request(app)
                .patch('/sections/' + sections[1]._id.toHexString()) 
                .send({ title, content, name })
                .expect(401)
                .end(err => {
                    if (err) {
                        return done(err)
                    }
    
                    Section.findOne({ _id: sections[1]._id.toHexString() })
                        .then(section => {
                            expect(section).toBeTruthy();
                            expect(section).not.toMatchObject({
                                title,
                                content,
                                name
                            });
                            done();
                        })
                        .catch(err => done(err));
                });
        });
  
        
    });
});