const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const mongoosePaginate = require('mongoose-paginate');

const SectionSchema = mongoose.Schema({
    title: {
        type: String,
        require: true,
        minlength: 2,
        trim: true,
    },
    content: {
        type: String,
        require: true,
        minlength: 2,
    },
    name: {
        type: String,
        require: true,
        minlength: 2,
    },

});

SectionSchema.plugin(mongoosePaginate);

SectionSchema.methods.toJSON = function() {
    const section = this;
    const sectionObject = section.toObject();

    return _.pick(sectionObject, ['_id', 'title', 'content', 'name']);
};

const Section = mongoose.model('Section', SectionSchema);

module.exports = {
    Section 
};