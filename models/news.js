const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const mongoosePaginate = require('mongoose-paginate');

const NewsSchema = mongoose.Schema({
    title: {
        type: String,
        require: true,
        minlength: 2,
        trim: true,
    },
    image: {
        type: String,
        require: true,
        minlength: 1,
        trim: true,
    },
    content: {
        type: String,
        require: true,
        minlength: 2,
    },
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        require: true
    }
});

NewsSchema.plugin(mongoosePaginate);

NewsSchema.methods.toJSON = function() {
    const news = this;
    const newsObject = news.toObject();

    return _.pick(newsObject, ['_id', 'title', 'content', 'image', '_creator']);
};

const News = mongoose.model('News', NewsSchema);

module.exports = {
    News
};