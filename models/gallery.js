const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const mongoosePaginate = require('mongoose-paginate');

const GallerySchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
        minlength: 2,
        trim: true,
    },
    description: {
        type: String,
        require: true,
        minlength: 2,
    },
    images: {
        type: [mongoose.Schema.Types.Mixed],
        require: true
    },
    _owner: {
        type: mongoose.Schema.Types.ObjectId,
        require: true
    }
});

GallerySchema.plugin(mongoosePaginate);

GallerySchema.methods.toJSON = function() {
    const gallery = this;
    const galleryObject = gallery.toObject();

    return _.pick(galleryObject, ['_id', 'name', 'description', '_owner', 'images']);
};

const Gallery = mongoose.model('Gallery', GallerySchema);

module.exports = {
    Gallery
};