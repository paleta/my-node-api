const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const mongoosePaginate = require('mongoose-paginate');

const UserSchema = mongoose.Schema({
    email: {
        type: String,
        set: email => email.toLowerCase(),
        require: true,
        trim: true,
        minlength: 3,
        unique: true,
        validate: {            
            validator: validator.isEmail,     
            message: '{VALUE} is not a valid e-mail.'          
        }
    },
    password: {
        type: String,
        require: true,
        minlength: 6
    },
    image: {
        type: String,
        require: true,
        minlength: 1,
        trim: true,
    },
    admin: {
        type: Boolean,
        default: false
    },
    description: {
        type: String,
        require: false,
        minlength: 1,
    },
    name: {
        type: String,
        require: false,
        minlength: 1,
        default: 'User'
    },
    tokens: [{
        access: {
            type: String,
            require: true
        },
        token: {
            type: String,
            require: true
        }
    }]
});

UserSchema.plugin(mongoosePaginate);


UserSchema.methods.toJSON = function() {
    const user = this;
    const userObject = user.toObject();

    return _.pick(userObject, ['_id', 'email', 'admin', 'description', 'image', 'name']);
};

UserSchema.methods.generateAuthToken = async function () {

    const user = this;
    const access = 'auth';

    const token = jwt.sign({
        _id: user._id.toHexString(),
        access
    }, process.env.JWT_SECRET).toString();

    user.tokens = user.tokens.concat([{access, token}]);

    try {
        await user.save();     
    } catch (error) {
        return;
    }

    return token;
};

UserSchema.methods.removeToken = async function(token) {

    const user = this;

    try {
        await user.update({
            $pull: {
                tokens: {
                    token
                }
            }
        });
    } catch (error) {
        return;
    }

    return;

};

UserSchema.statics.findByToken = async function(token) {

    const User = this;
    let decoded;

    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
        return await User.findOne({
            '_id': decoded._id,
            'tokens.token': token,
            'tokens.access': 'auth'
        });

    } catch (err) {
        return;
    }
    
};

UserSchema.statics.findByCredentials = async function(email, password) {

    const User = this;

    try {
        const user = await User.findOne({ email });
        const match = await bcrypt.compare(password, user.password);

        if (match) {
            return user;
        }

    } catch (error) {       
        return;
    }

    return;

};

UserSchema.pre('save', async function(next) {
    const user = this;

    if (user.isModified('password')) {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(user.password, salt);
        user.password = hash;
        next();
    } else {
        next();
    }

});

const User = mongoose.model('User', UserSchema);

module.exports = {
    User 
};