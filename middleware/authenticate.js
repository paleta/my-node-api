const { User } = require('./../models/user');

const authenticate = async (req, res, next) => {

    const token = req.header('X-Auth');

    try {
        const user = await User.findByToken(token);
        if (!user) {
            throw new Error();
        }
        req.user = user;
        req.token = token;
        next();

    } catch (error) {
        res.status(401).send();
    }
    
};

module.exports = {
    authenticate
};