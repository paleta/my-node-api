require('./config/config');

const express = require('express');
const fileUpload = require('express-fileupload');
const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const _ = require('lodash');
const path = require('path');
const fs = require('fs');

const { User } = require('./models/user');
const { Section } = require('./models/section');
const { News } = require('./models/news');
const { Gallery } = require('./models/gallery');
const { authenticate } = require('./middleware/authenticate');

const app = express();
const port = process.env.PORT;

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });

app.use(bodyParser.json());
app.use(fileUpload());

// USERS
// POST /users
app.post('/users', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to register new user!'); 
        }

        if(!req.files || !req.body.email || !req.body.password) {
            throw new Error('All fields are required');
        }
    
        const body = _.pick(req.body, ['email', 'password']);
    
        const image = req.files.image;
        const imageName = `${new ObjectID().toHexString()}_${image.name}`;
        const imageLink = `/images/${imageName}`;
    
        const user = new User({
            email: body.email,
            password: body.password,
            image: imageLink
        }); 

        await Promise.all([
            image.mv(path.join(__dirname, imageLink)),
            user.save()
        ])

        const token = await user.generateAuthToken(); // test if token is a string, NOT a promise

        res.header('X-Auth', token).send(user);
    } catch (err) {
        res.status(400).send(err.message);
    }

});

//GET /users - it should return all users

app.get('/users', async (req, res) => {
  
    try {

        const page = req.query.page ? +req.query.page : 1;
        const sort = req.query.sort ? req.query.sort : 'desc';
        const limit = req.query.limit ? +req.query.limit : 20;

        const users = await User.paginate({}, { page: page, sort: { _id: sort }, limit: limit });

        res.status(200).send({
            users: users.docs,
            meta: {
                total: users.total,
                page: users.page,
                pages: users.pages,
                limit: users.limit
            }
        });

    } catch (error) {
        return res.status(400).send();
    }

});

// GET /users/me
app.get('/users/me', authenticate, async (req, res) => {
    res.send(req.user);
});

// POST /users/login
app.post('/users/login', async (req, res) => {

    try {
        const body = _.pick(req.body, ['email', 'password']);
        const user = await User.findByCredentials(body.email, body.password);
        
        if(!user) {
            throw new Error('Bad credentials');
        }

        const token = await user.generateAuthToken();

        res.header('X-Auth', token).send(user);


    } catch (error) {
        return res.status(400).send(error.message);
    }

});

// DELETE /users/me/token
app.delete('/users/me/token', authenticate, async (req, res) => {

    try {
        await req.user.removeToken(req.token)
        res.status(200).send();

    } catch (error) {
        return res.status(400).send();
    }

});

// DELETE /users/me
app.delete('/users/me', authenticate, async (req, res) => {

    try {
        const user = await User.findOneAndRemove({_id: req.user._id});

        if(!user) {
            throw new Error('User NOT found');
        }
        
        fs.unlink(path.join(__dirname, user.image), (err) => {
            if (err) {
                console.log(err);         
            }
        });

        const galleries = await Gallery.find({ _owner: user._id });

        if (galleries) {
            
            galleries.forEach( async (gallery) => {

                gallery.images.forEach(image => {
                    fs.unlink(path.join(__dirname, image.link), (err) => {
                        if (err) {
                            console.log(err);         
                        }
                    });
                });
    
                await gallery.remove();
            });
        }


        res.send(req.user);

    } catch (error) {
        res.status(400).send(err.message);
    }
    
});

// PATCH /users/me/update
app.patch('/users/me/update', authenticate, async (req, res) => {

    try {

        const body = _.pick(req.body, ['email', 'description', 'name', 'password']);
        const user = req.user;

        let image;
        if (req.files && req.files.image) {      
            
            fs.unlink(path.join(__dirname, user.image), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            image = req.files.image;
            const imageName = `${new ObjectID().toHexString()}_${image.name}`;
            const imageLink = `/images/${imageName}`;
            
            body.image = imageLink;

        }

        await user.set(body);
        await user.save();
        if (image) {
            await image.mv(path.join(__dirname, body.image));
        }
    
        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }
    
});

// DELETE /users/:id
app.delete('/users/:id', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to delete this user');
        }
    
        const user = await User.findOneAndRemove({_id: req.params.id});

        if(!user) {
            throw new Error('User NOT found');
        }

        fs.unlink(path.join(__dirname, user.image), (err) => {
            if (err) {
                console.log(err);         
            }
        });

        const galleries = await Gallery.find({ _owner: user._id });

        if (galleries) {

            galleries.forEach( async (gallery) => {

                gallery.images.forEach(image => {
                    fs.unlink(path.join(__dirname, image.link), (err) => {
                        if (err) {
                            console.log(err);         
                        }
                    });
                });
    
                await gallery.remove();
            });
        }



        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /users/:id
app.patch('/users/:id', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to update this user');
        }

        const body = _.pick(req.body, ['email', 'admin', 'description', 'name', 'password']);

        const user = await User.findOne({_id: req.params.id});

        if(!user) {
            throw new Error('User NOT found');
        }

        let image;
        if (req.files && req.files.image) {      
            
            fs.unlink(path.join(__dirname, user.image), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            image = req.files.image;
            const imageName = `${new ObjectID().toHexString()}_${image.name}`;
            const imageLink = `/images/${imageName}`;
            
            body.image = imageLink;

        }

        await user.set(body);
        await user.save();
        if (image) {
            await image.mv(path.join(__dirname, body.image));
        }

        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }

});


// GET /users/:id
app.get('/users/:id', async (req, res) => {

    try {
    
        const user = await User.findOne({_id: req.params.id});

        if(!user) {
            throw new Error('User NOT found');
        }

        res.send(user);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// SECTIONS
// POST /sections
app.post('/sections', authenticate, async (req, res) => {

    try {
    
        if (!req.user.admin) {
            throw new Error('You do NOT have premission to add new section');
        }

        const body = _.pick(req.body, ['title', 'content', 'name']);
        const section = new Section(body); 
        await section.save();

        res.send(section);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// GET /sections
app.get('/sections', async (req, res) => {

    try {

        const page = req.query.page ? +req.query.page : 1;
        const sort = req.query.sort ? req.query.sort : 'desc';
        const limit = req.query.limit ? +req.query.limit : 100;

        const sections = await Section.paginate({}, { page: page, sort: { _id: sort }, limit: limit });

        res.status(200).send({
            sections: sections.docs,
            meta: {
                total: sections.total,
                page: sections.page,
                pages: sections.pages,
                limit: sections.limit
            }
        });

    } catch (error) {
        return res.status(400).send();
    }

});


// DELETE /sections
app.delete('/sections/:id', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to delete this section');
        }
    
        const section = await Section.findOneAndRemove({_id: req.params.id});

        if(!section) {
            throw new Error('Section NOT found');
        }

        res.send(section);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /sections/:id
app.patch('/sections/:id', authenticate, async (req, res) => {

    try {

        if (!req.user.admin) {
            throw new Error('You do NOT have premission to patch this section');
        }

        const body = _.pick(req.body, ['title', 'content', 'name']);
    
        const section = await Section.findOneAndUpdate({ _id: req.params.id }, { $set: body }, { new: true });

        if(!section) {
            throw new Error('Section NOT found');
        }

        res.send(section);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// NEWS
// POST /news
app.post('/news', authenticate, async (req, res) => {

    try {
    
        if(!req.files || !req.body.title || !req.body.content) {
            throw new Error('All fields are required');
        }
    
        const body = _.pick(req.body, ['title', 'content']);
    
        const image = req.files.image;
        const imageName = `${new ObjectID().toHexString()}_${image.name}`;
        const imageLink = `/images/${imageName}`;
    
        const news = new News({
            title: body.title,
            content: body.content,
            image: imageLink,
            _creator: req.user._id
        }); 

        await Promise.all([
            image.mv(path.join(__dirname, imageLink)),
            news.save()
        ]);

        res.send(news);

    } catch (err) {
        res.status(400).send(err.message);
    }

});


// GET /news

app.get('/news', async (req, res) => {
  
    try {

        const page = req.query.page ? +req.query.page : 1;
        const sort = req.query.sort ? req.query.sort : 'desc';
        const limit = req.query.limit ? +req.query.limit : 20;

        const news = await News.paginate({}, { page: page, sort: { _id: sort }, limit: limit });

        res.status(200).send({
            heroSlides: news.docs,
            meta: {
                total: news.total,
                page: news.page,
                pages: news.pages,
                limit: news.limit
            }
        });

    } catch (error) {
        return res.status(400).send();
    }

});


// GET /news/:id
app.get('/news/:id', async (req, res) => {

    try {
    
        const news = await News.findOne({ _id: req.params.id });
        const creator = await User.findOne({ _id: news._creator });
        const gallery = await Gallery.find({ _owner: news._id });

        if(!news) {
            throw new Error('News NOT found');
        }

        res.send({
            news,
            creator,
            gallery
        });

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /news/:id
app.patch('/news/:id', authenticate, async (req, res) => {

    try {

        const body = _.pick(req.body, ['title', 'content']);
        let news;

        if (req.user.admin) {
            news = await News.findOne({ _id: req.params.id });
        } else {
            news = await News.findOne({ _id: req.params.id, _creator: req.user._id });
        }

        if(!news) {
            throw new Error('News NOT found');
        }

        let image;
        if (req.files && req.files.image) {      
            
            fs.unlink(path.join(__dirname, news.image), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            image = req.files.image;
            const imageName = `${new ObjectID().toHexString()}_${image.name}`;
            const imageLink = `/images/${imageName}`;
            
            body.image = imageLink;

        }

        await news.set(body);
        await news.save();
        if (image) {
            await image.mv(path.join(__dirname, body.image));
        }

        res.send(news);

    } catch (err) {
        res.status(400).send(err.message);
    }

});


// DELETE /news/:id
app.delete('/news/:id', authenticate, async (req, res) => {

    try {

        if (req.user.admin) {
            news = await News.findOneAndRemove({ _id: req.params.id });
        } else {
            news = await News.findOneAndRemove({ _id: req.params.id, _creator: req.user._id });
        }

        if (!news) {
            throw new Error('News NOT found');
        }

        const gallery = await Gallery.findByIdAndRemove({ _owner: news._id });

        const galleries = await Gallery.find({ _owner: news._id });

        if (galleries) {
            galleries.forEach( async (gallery) => {

                gallery.images.forEach(image => {
                    fs.unlink(path.join(__dirname, image.link), (err) => {
                        if (err) {
                            console.log(err);         
                        }
                    });
                });
    
                await gallery.remove();
            });
        }

        fs.unlink(path.join(__dirname, news.image), (err) => {
            if (err) {
                console.log(err);
            }
        });

        res.send(news);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// GALLERY
// POST /gallery?owner=owner_id
app.post('/gallery', authenticate, async (req, res) => {

    try {
  
        if (!req.files || !req.body.name || !req.body.description) {
            throw new Error('All fields are required');
        }
    
        const body = _.pick(req.body, ['name', 'description']);

        let images = [];

        for (const imageKey in req.files) {
            if (req.files.hasOwnProperty(imageKey)) {
                
                const image = req.files[imageKey];
                const imageName = `${new ObjectID().toHexString()}_${image.name}`;
                const imageLink = `/images/${imageName}`;
                images = images.concat([{
                    name: imageName,
                    link: imageLink
                }]);
                await image.mv(path.join(__dirname, imageLink));

            }
        }
   
        const gallery = new Gallery({
            name: body.name,
            description: body.description,
            images: images,
            _owner: req.query.owner ? req.query.owner : req.user._id
        }); 
               
        await gallery.save();

        res.send(gallery);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /gallery/title-and-description/:id
app.patch('/gallery/title-and-description/:id', authenticate, async (req, res) => {

    try {

        const body = _.pick(req.body, ['name', 'description']);
        let gallery;

        if (req.user.admin) {
            gallery = await Gallery.findOneAndUpdate({ _id: req.params.id }, { $set: body }, { new: true });
        } else {
            gallery = await Gallery.findOneAndUpdate({ _id: req.params.id, _owner: req.user._id }, { $set: body }, { new: true });
        }
        
        if(!gallery) {
            throw new Error('Gallery NOT found');
        }

        res.send(gallery);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /gallery/new-image/:id
app.patch('/gallery/new-image/:id', authenticate, async (req, res) => {

    try {

        let gallery;
        if (req.user.admin) {
            gallery = await Gallery.findOne({ _id: req.params.id });
        } else {
            gallery = await Gallery.findOne({ _id: req.params.id, _owner: req.user._id });
        }


        if(!gallery) {
            throw new Error('Gallery NOT found');
        }

        if (!req.files) {  
           throw new Error('Image files are required');
        }

        let images = gallery.images;

        for (const imageKey in req.files) {
            if (req.files.hasOwnProperty(imageKey)) {
                
                const image = req.files[imageKey];
                const imageName = `${new ObjectID().toHexString()}_${image.name}`;
                const imageLink = `/images/${imageName}`;
                images = images.concat([{
                    name: imageName,
                    link: imageLink
                }]);
                await image.mv(path.join(__dirname, imageLink));
            }
        }
        

        await gallery.set('images', images);
        await gallery.save();

        res.send(gallery);

    } catch (err) {
        res.status(400).send(err.message);
    }

});

// PATCH /gallery/delete-image/:id?image=image_id
app.patch('/gallery/delete-image/:id', authenticate, async (req, res) => {

    try {

        let gallery;
        if (req.user.admin) {
            gallery = await Gallery.findOne({ _id: req.params.id });
        } else {
            gallery = await Gallery.findOne({ _id: req.params.id, _owner: req.user._id });
        }


        if(!gallery) {
            throw new Error('Gallery NOT found');
        }

        const imageIndex = gallery.images.findIndex(image => image.name === req.query.image);
        const image = gallery.images.find(image => image.name === req.query.image);

        if (image) {

            fs.unlink(path.join(__dirname, image.link), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            gallery.images.splice(imageIndex, 1);
   
            await gallery.set(gallery);
            await gallery.save();
    
            res.send(gallery);
        } else {
            throw new Error('Image Not found');
        }

    } catch (err) {
        res.status(400).send(err.message);
    }

});


// PATCH /gallery/update-image/:id?image=image_id

app.patch('/gallery/update-image/:id', authenticate, async (req, res) => {

    try {

        let gallery;
        if (req.user.admin) {
            gallery = await Gallery.findOne({ _id: req.params.id });
        } else {
            gallery = await Gallery.findOne({ _id: req.params.id, _owner: req.user._id });
        }

        if(!gallery) {
            throw new Error('Gallery NOT found');
        }

        if (!req.files) {
            throw new Error('Image file is required');
        }

        const imageIndex = gallery.images.findIndex(image => image.name === req.query.image);
        const image = gallery.images.find(image => image.name === req.query.image);

        if (image) {

            fs.unlink(path.join(__dirname, image.link), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

            const newImage = req.files.image;
            const imageName = `${new ObjectID().toHexString()}_${newImage.name}`;
            const imageLink = `/images/${imageName}`;

            gallery.images.splice(imageIndex, 1, {
                name: imageName,
                link: imageLink
            });

            await newImage.mv(path.join(__dirname, imageLink)); 

            await gallery.set(gallery);
            await gallery.save();
    
            res.send(gallery);

        } else {
            throw new Error('Image Not found');
        }

    } catch (err) {
        res.status(400).send(err.message);
    }

});


app.get('/gallery', async (req, res) => {
  
    try {

        const page = req.query.page ? +req.query.page : 1;
        const sort = req.query.sort ? req.query.sort : 'desc';
        const limit = req.query.limit ? +req.query.limit : 20;

        const gallery = await Gallery.paginate({}, { page: page, sort: { _id: sort }, limit: limit });

        res.status(200).send({
            galleries: gallery.docs,
            meta: {
                total: gallery.total,
                page: gallery.page,
                pages: gallery.pages,
                limit: gallery.limit
            }
        });

    } catch (error) {
        return res.status(400).send();
    }

});


// GET /gallery/:id
app.get('/gallery/:id', async (req, res) => {

    try {
    
        const gallery = await Gallery.findOne({ _id: req.params.id });

        if(!gallery) {
            throw new Error('News NOT found');
        }

        let creator = await User.findOne({ _id: gallery._owner });

        if (!creator) {
            creator = await News.findOne({ _id: gallery._owner });
        }

        res.send({
            gallery,
            creator
        });

    } catch (err) {
        res.status(400).send(err.message);
    }

});


// DELETE /gallery/:id
app.delete('/gallery/:id', authenticate, async (req, res) => {

    try {

        let gallery;
        if (req.user.admin) {
            gallery = await Gallery.findOneAndRemove({ _id: req.params.id });
        } else {
            gallery = await Gallery.findOneAndRemove({ _id: req.params.id, _owner: req.user.id });
        }

        if (!gallery) {
            throw new Error('Gallery NOT found');
        }

            
        gallery.images.forEach(img => {

            fs.unlink(path.join(__dirname, img.link), (err) => {
                if (err) {
                    console.log(err);         
                }
            });

        });
     
        res.send(gallery);

    } catch (err) {
        res.status(400).send(err.message);
    }

});




// GET /images/:name - it should return image of a given name
app.get('/images/:name', async (req, res) => {

    const filePath = path.join(__dirname, 'images', req.params.name);

    try {
        fs.stat(filePath, (err, stats) => {
            if (err) {
                //return res.status(404).send('File does NOT exist!');
                throw new Error('dihvdhuv');
            } else {
                res.sendFile(filePath);
            }
        });
        
    } catch (error) {
        res.status(400).send();
    }

});


if(!module.parent){ 
    app.listen(port); 
}

module.exports = { app };