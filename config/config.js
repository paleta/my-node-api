const env = process.env.NODE_ENV || 'development';
console.log('**** env: ', env, '****');

if (env === 'development' || env === 'test') {
    const config = require('./config.json');
    const envConfig = config[env];

    for (const key in envConfig) {
        if (envConfig.hasOwnProperty(key)) {       
            process.env[key] = envConfig[key];
        }
    }
}
